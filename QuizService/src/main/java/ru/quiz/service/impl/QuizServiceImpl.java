package ru.quiz.service.impl;

import org.springframework.stereotype.Service;
import ru.quiz.entities.Quiz;
import ru.quiz.repositories.QuizRepository;
import ru.quiz.service.QuestionClient;
import ru.quiz.service.QuizService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QuizServiceImpl implements QuizService {
    private QuizRepository quizRepository;
    private QuestionClient questionClient;

    public QuizServiceImpl(QuizRepository quizRepository, QuestionClient questionClient) {
        this.quizRepository = quizRepository;
        this.questionClient = questionClient;
    }

    @Override
    public Quiz add(Quiz quiz) {
        return quizRepository.save(quiz);
    }

    @Override
    public List<Quiz> get() {
        List<Quiz> all = quizRepository.findAll();
        all
                .stream()
                .map(quiz -> {
                    quiz.setQuestions(questionClient.getQuestionsByQuizId(quiz.getId()));
                    return quiz;
                }).collect(Collectors.toList());
        return all;
    }

    @Override
    public Quiz get(Long id) {
        Quiz quiz = quizRepository.findById(id).orElse(null);
        quiz.setQuestions(questionClient.getQuestionsByQuizId(quiz.getId()));
        return quiz;
    }
}
