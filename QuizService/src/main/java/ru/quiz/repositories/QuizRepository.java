package ru.quiz.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.quiz.entities.Quiz;

public interface QuizRepository extends JpaRepository<Quiz, Long> {
}
