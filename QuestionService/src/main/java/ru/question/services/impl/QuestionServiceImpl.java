package ru.question.services.impl;

import org.springframework.stereotype.Service;
import ru.question.entities.Question;
import ru.question.repositories.QuestionRepository;
import ru.question.services.QuestionService;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
   private QuestionRepository questionRepository;

    public QuestionServiceImpl(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    @Override
    public Question create(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public List<Question> get() {
        return questionRepository.findAll();
    }

    @Override
    public Question getOne(Long id) {
        return questionRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Question not found with id"));
    }

    @Override
    public List<Question> getQuestionsByQuizId(Long quizId) {
        return questionRepository.findByQuizId(quizId);
    }
}
