package ru.question.controllers;

import org.springframework.web.bind.annotation.*;
import ru.question.entities.Question;
import ru.question.services.QuestionService;

import java.util.List;

@RestController
@RequestMapping("/question")
public class QuestionController {
    private final QuestionService questionService;

    public QuestionController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @PostMapping
    public Question create(@RequestBody Question question) {
        return questionService.create(question);
    }
    @GetMapping
    public List<Question> get() {
        return questionService.get();
    }
    @GetMapping("/{id}")
    public Question getOne(@PathVariable Long id) {
        return questionService.getOne(id);
    }
    @GetMapping("/quiz/{quizId}")
    public List<Question> getQuestionsByQuizId(@PathVariable Long quizId) {
        return questionService.getQuestionsByQuizId(quizId);
    }
}
