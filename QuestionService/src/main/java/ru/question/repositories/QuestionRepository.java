package ru.question.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.question.entities.Question;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> findByQuizId(Long quizId);
}
